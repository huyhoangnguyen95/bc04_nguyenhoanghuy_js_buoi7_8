var numArr = [];

function themSo() {
    // console.log(123);
    var num = document.getElementById('txt-numN').value * 1;
    document.getElementById('txt-numN').value = "";
    numArr.push(num);
    // console.log('numArr: ', numArr);
    document.getElementById("resultArr").innerHTML = ` ${numArr} `;
    laySoDuong();
}


function laySoDuong() {
    // console.log(123);
    var mangSoDuong = numArr.filter(function (item) {
        return item >= 0;
    });
    console.log('mangSoDuong: ', mangSoDuong);
    return mangSoDuong;
}

function tinhTong() {
    var sum = 0;
    var count = 0;
    var mangSoDuong = laySoDuong();
    mangSoDuong.forEach(function (item) {
        sum += item;
        count++;
    });
    // console.log('sum: ', sum);
    document.getElementById("resultBai1").innerHTML = ` Tổng số dương : ${sum} `;
    return count;
}

function demSo() {
    // console.log(123);
    var count = tinhTong();
    document.getElementById("resultBai2").innerHTML = ` Số dương : ${count} `;
}

function timSoNhoNhat() {
    // console.log(123);
    var soNhoNhat = numArr[0];
    numArr.forEach(function (item) {
        if (item < soNhoNhat) {
            soNhoNhat = item;
        }
    })
    // console.log('soNhoNhat: ', soNhoNhat);
    document.getElementById("resultBai3").innerHTML = ` Số nhỏ nhất : ${soNhoNhat} `;
}

function timSoDuongNhoNhat() {
    // console.log(123);
    var mangSoDuong = laySoDuong();
    var soDuongNhoNhat = mangSoDuong[0];
    if (mangSoDuong.length !== 0) {
        // console.log("có phần tử");
        mangSoDuong.forEach(function (item) {
            if (item < soDuongNhoNhat) {
                soDuongNhoNhat = item;
            }
        });
        // console.log('soDuongNhoNhat: ', soDuongNhoNhat);
        document.getElementById("resultBai4").innerHTML = ` Số dương nhỏ nhất : ${soDuongNhoNhat} `;

    } else {
        // console.log("KHông có số dương");
        document.getElementById("resultBai4").innerHTML = ` Không có số dương`;
    }
}

function timSoChanCuoiCung() {
    // console.log(123);
    var soChanCuoiCung;
    numArr.forEach(function (item) {
        if (item % 2 == 0) {
            soChanCuoiCung = item;
        }
    })
    // console.log('soChanCuoiCung: ', soChanCuoiCung);
    document.getElementById("resultBai5").innerHTML = ` Số chẵn cuối cùng : ${soChanCuoiCung} `;

}

function sapXep() {
    // console.log(123);
    var numArrTang = numArr.sort();
    // console.log('numArrTang: ', numArrTang);
    document.getElementById("resultBai7").innerHTML = ` Mảng sau khi sắp xếp : ${numArrTang} `;

}

function soSanh() {
    // console.log(123);
    var soDuong = 0;
    var soAm = 0;
    numArr.forEach(function (item) {
        if (item > 0) {
            soDuong++;
        } else {
            soAm++;
        }
    });
    if (soDuong == soAm) {
        // console.log("bằng");
        document.getElementById("resultBai10").innerHTML = ` Số dương bằng số âm `;
    } else if (soDuong > soAm) {
        // console.log("lớn");
        document.getElementById("resultBai10").innerHTML = ` Số dương lớn hơn số âm `;
    } else {
        // console.log("nhỏ");
        document.getElementById("resultBai10").innerHTML = ` Số dương nhỏ hơn số âm `;
    }
}